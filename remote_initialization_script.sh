#!/bin/bash

lockfile=".tamar_configuration_lockfile"

# Check is the sever is already configured
if [[ -e $lockfile ]] ; then
	read -p "The machine was already configured. Reconfigure? " -n 1 -r
	echo
	if ! [[ $REPLY =~ [yY] ]] ; then
		exit 0
	fi
fi

touch "$lockfile"

echo "Running initialization script"

 mkdir -p /mnt/lamer
 mount -t cifs -r -o vers=3.0,user=lkpo,password=lkpo1234ftw! //10.10.10.10/Public /mnt/lamer

 yum install -y tree
