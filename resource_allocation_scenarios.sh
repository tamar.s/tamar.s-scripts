RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
PURPLE='\033[0;35m'
NC='\033[0m' # No Color

is_sensor=false
networks_count=1

printf "\n${YELLOW}Initial status:${NC}\n"
printf "${YELLOW}preprocessor_count = $(lkpocli manager api ranger count_preprocessors)\n"
printf "${YELLOW}dissectors_count = $(lkpocli manager api dissectors count_dissectors)\n"
printf "${YELLOW}dissectors_ng_count = $(lm conf | jq ".dissector_ng.dissectors_threads_count")\n"

function test0(){
	printf "\n${PURPLE}Test case 0 - rebootstrap the machine trigger calculation${NC}\n"
	echo "Before re-bootstrap status"
	./resources_verification.sh $is_sensor $networks_count
	lm freset
	sleep 30
	lkpocli manager api configurator bootstrap_site
	sleep 30
	echo "After re-bootstrap status"
	./resources_verification.sh $is_sensor $networks_count
}

function test1(){
	printf "\n${PURPLE}Test case 1 - check allocation on initial machine${NC}\n"
	./resources_verification.sh $is_sensor $networks_count
}

function test2(){
	printf "\n${PURPLE}Test case 2 - add network reallocates resources${NC}\n"
	lm add_n sec_net
	sleep 30
	networks_count=$(($networks_count+1))
	./resources_verification.sh $is_sensor $networks_count
}

function test3(){
	printf "\n${PURPLE}Test case 3 - removes network reallocates resources${NC}\n"
	lm rm_n sec_net
	sleep 30
	networks_count=$(($networks_count-1))
	./resources_verification.sh $is_sensor $networks_count
} 

function test4(){
	printf "\n${PURPLE}Test case 4 - manual configure dissectors and then trigering the automatic reallocations resources${NC}\n"
	lm add_d
	lm add_pp
	sleep 30
	echo "after manually adding the dissectors and preprocessors - should fail"
	./resources_verification.sh $is_sensor $networks_count
	lkpocli manager api ranger configure_dissectors_preprocessors
	echo "after manually triggering the automatic allocation"
	sleep 30 
	./resources_verification.sh $is_sensor $networks_count
}

function test5(){
	printf "\n${PURPLE}Test case 5 - add 2 networks reallocates resources${NC}\n"
	lm add_n sec_net
	lm add_n third_net
	sleep 30
	networks_count=$(($networks_count+2))
	./resources_verification.sh $is_sensor $networks_count
}

function test6(){
	printf "\n${PURPLE}Test case 6 - remove 2 networks reallocates resources${NC}\n"
	lm rm_n sec_net
	lm rm_n third_net
	sleep 30
	networks_count=$(($networks_count-2))
	./resources_verification.sh $is_sensor $networks_count
}

function test7(){
	printf "\n${PURPLE}Test case 7 - adding sensor reallocates resources${NC}\n"
	bootstrap_password=$(lm conf | jq -r ".community.bootstrap_password")
	read -p "Enter sensor's ip: " sensor_ip
	read -p "Enter sensor's name: " sensor_name
	site_ip=$(ifconfig | grep -o "inet [^ ]*" | grep -o "10\.91.*")
	ssh root@$sensor_ip "lkpocli manager api configurator bootstrap_sensor $sensor_name $site_ip $bootstrap_password"
	sleep 30
	./resources_verification.sh $is_sensor $networks_count
}


function test8(){
	printf "\n${PURPLE}Test case 8 - remove sensor reallocates resources${NC}\n"
	bootstrap_password=$(lm conf | jq -r ".community.bootstrap_password")
	read -p "Enter sensor's name: " sensor_name
	lkpocli manager api configurator remove_sensor $sensor_name
	sleep 30
	./resources_verification.sh $is_sensor $networks_count
}

#test0
test1
test2
test3
test4
test5
test6
test7
test7
test8
