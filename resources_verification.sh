if [ $# -ne 2 ] ; then
	echo "Missings arguments. 2 Arguments required"
	exit 1
fi

RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

total_cpu=$(nproc)
total_memory_gb=$(python -c "print($(grep MemTotal /proc/meminfo | awk '{print $2}') /1024.0/1024.0)")
is_sensor=$1
remote_sensors_count=$(lm conf | jq ".sensors | length")
networks_count=$2

printf "${YELLOW}CPU${NC}=$total_cpu, ${YELLOW}RAM${NC}=$total_memory_gb, ${YELLOW}SENSORS_NUM${NC}=$remote_sensors_count, ${YELLOW}NETWORK_COUNT${NC}=$networks_count\n"

result=$(lkpocli manager api ranger calc_dissectors_preprocessors total_cpu=$total_cpu total_memory_gb=$total_memory_gb is_sensor=$is_sensor remote_sensors_count=$remote_sensors_count networks_count=$networks_count)

expected_py_dissc=$(echo $result | grep -o  "py_dissectors=[0-9]*" | grep -o "[0-9]*")
expected_ng_dissc=$(echo $result | grep -o  "ng_dissectors=[0-9]*" | grep -o "[0-9]*")
expected_preproce=$(echo $result | grep -o  "preprocessors=[0-9]*" | grep -o "[0-9]*")

actual_preproce=$(lkpocli manager api ranger count_preprocessors)
actual_py_dissc=$(lkpocli manager api dissectors count_dissectors)
actual_ng_dissc=$(lm conf | jq ".dissector_ng.dissectors_threads_count")

printf "${YELLOW}expected_py_dissc: $expected_py_dissc | actual_py_dissc: $actual_py_dissc${NC}\n"
printf "${YELLOW}expected_ng_dissc: $expected_ng_dissc | actual_ng_dissc: $actual_ng_dissc${NC}\n"
printf "${YELLOW}expected_preproce: $expected_preproce | actual_preproce: $actual_preproce${NC}\n"

if [ "$expected_py_dissc" = "$actual_py_dissc" ] && [ "$expected_ng_dissc" = "$actual_ng_dissc" ] && [ "$expected_preproce" = "$actual_preproce" ] ; then
	printf "${GREEN}Allocations as expected${NC}\n"
else
	printf "${RED}Allocation didn't match expectation${NC}\n"
fi


