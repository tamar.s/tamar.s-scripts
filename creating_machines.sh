#!/bin/bash
echo "Please specify version"
read ver
echo;
echo "Please specify the dump path: something like BHP/dump_2021"
read path
dump_dir=$path
echo ;
echo "Specify base_name "
read bname
base_name=$bname
echo ;
echo "specify model"
read module
echo ;
echo "Please specify host expiration"
options=("7" "14" "Quit")
select Eopt in "${options[@]}"
do
    case $Eopt in
        "7")
            expiration=7
                        break
            ;;
        "14")
            expiration=14
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo ;
echo "Select how much ram do you need "
options=("8gb" "16gb" "Quit")
select Ropt in "${options[@]}"
do
    case $Ropt in
        "8gb")
            Ram=8096
                        break
            ;;
        "16gb")
            Ram=16192
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo;
echo "Select how much CPU do you need "
options=("8cpu" "16cpu" "Quit")
select Copt in "${options[@]}"
do
    case $Copt in
        "8cpu")
            cpu=8
                        break
            ;;
        "16cpu")
            cpu=16
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo ;
echo " which Vsphere you prefer"
options=("vSphere1" "vSphere2" "Quit")
select Vsopt in "${options[@]}"
do
    case $Vsopt in
        "vSphere1")
            vs=vsphere1
                        break
            ;;
        "vSphere2")
            vs=vsphere2
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo ;
echo " How much space you need"
options=("40" "70" "Quit")
select Vdisk in "${options[@]}"
do
    case $Vdisk in
        "40")
            disk=40
                        break
            ;;
        "70")
            disk=70
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo "Should we connect Cloud?"
options=("Yes" "No" "Quit")
select Vdisk in "${options[@]}"
do
    case $Vdisk in
        "Yes")
            cloud=True
                        break
            ;;
        "No")
            cloud=False
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo "Should we Bootstrap?"
options=("Yes" "No" "Quit")
select Vdisk in "${options[@]}"
do
    case $Vdisk in
        "Yes")
            BS=True
                        break
            ;;
        "No")
            BS=False
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo ;
echo "We will be build the follwoing" :
echo ;
build="master model template build $module ranger_from_dump backing=$vs bootstrap=$BS dump_dir=$path enable_cloud=$cloud host_cpus=$cpu host_disk=$disk host_ram=$Ram host_expiration=$expiration installer_version=$ver base_name=$bname"
echo $build
options=("Yes"  "Quit")
select D in "${options[@]}"
do
    case $D in
        "Yes")
                         master model template build $module ranger_from_dump backing=$vs bootstrap=$BS dump_dir=$path enable_cloud=$cloud host_cpus=$cpu host_disk=$disk host_ram=$Ram host_expiration=$expiration installer_version=$ver base_name=$bname
                        break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done