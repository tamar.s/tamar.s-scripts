#!/bin/zsh

cd ~/Scripts

user="root"

if [[ -z $1 ]] ; then
	# Read IP from client
	read "IP?Please enter server IP you wish to configure: " 
else
	IP=$1
fi

# Check if IP is reachable using ping
if [[ ! $(ping -c 1 ${IP}) ]] >& /dev/null ; then
	echo "Failed to reach ${IP}"
	exit 1
fi

creds=${user}@${IP}

# Install an ssh key
if ! ssh-copy-id ${creds} >& /dev/null ; then
	echo "ssh-copy-id failed"
	exit 1
fi

# Check initializtion file exists
initialization_script="remote_initialization_script.sh"
if ! [[ -e "${initialization_script}" ]] ; then
	echo "initialization script '${initialization_script}' is not found"
	exit 1
fi

# Copy and Run the initialization file on the remote server
rsync ${initialization_script} ${creds}:~/
ssh -tq ${creds} bash ./${initialization_script}

# Copy the aliases file 
aliases_filename="remote_machine_aliases.sh"
rsync ${aliases_filename} ${creds}:~/

# Copy resources allocation qa-automation file
rsync resources_verification.sh ${creds}:~/
rsync resource_allocation_scenarios.sh ${creds}:~/

# Source the aliases file from .bashrc
ssh -q ${creds} "echo \"source ${aliases_filename}\" >> ~/.bashrc" 

ssh ${creds}
