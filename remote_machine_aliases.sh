#!/bin/bash

# Check is the sever was already configured
[[ "${TAMAR_ALIASES}" == "yes" ]] && return 0
TAMAR_ALIASES=yes

function latest(){
	set size="$1"
	if [[ -z "$size" ]] ; then
		size=10
	fi

	ls -t /mnt/lamer/Deployment/linux_installers/ranger/centos/*.tar.gz | head -n $size | grep -o --color=always "CTD-.*" | sort
}

function crv(){
	rsync -Pa /mnt/lamer/Deployment/linux_installers/ranger/rhel/CTD-$1-Installer-RHEL7.9.tar.gz .
}

 function ccv(){
         rsync -Pa /mnt/lamer/Deployment/linux_installers/ranger/centos/CTD-$1-Installer-CentOS7.9.tar.gz .
 }

function ceth(){
	ip link add $1 type dummy;
	ip link set up dev $1;
	lm eth $1;
	ifconfig $1 mtu 65000;
}

function findi(){
	find . -iname "*%1*"
}

function set_na(){
	lm set_config workers.active.use_active_from_sensor true
	lm da
	lm ea
}

function res_count(){
 	echo -n "	count_preprocessors:"
	lkpocli manager api ranger count_preprocessors
	echo -n "	count_dissector:"
	lkpocli manager api dissectors count_dissectors
	lm conf | grep --color=never dissectors_threads_count
}

alias conductor="ssh tamar.s@conductor.t82.co"
alias wt='watch -n 1 " find . | sed -e \"s/[^-][^\/]*\//  \|/g\" -e \"s/\|\([^ ]\)/\|-\1/\" "'
